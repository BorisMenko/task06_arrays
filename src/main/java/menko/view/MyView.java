package menko.view;


import menko.controller.Task1;
import menko.controller.Task2;
import menko.controller.Task3;
import menko.controller.Task4.GameArray;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {


    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);


    public MyView() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Task A");
        menu.put("2", "  2 - Task B");
        menu.put("3", "  3 - Task 3");
        menu.put("4", "  4 - Task 4");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {

        Task1 task1 = new Task1();
        task1.view();
    }

    private void pressButton2() {
        Task2.view();
    }

    private void pressButton3() {
        Task3.deleteElementWhichMeetMoreThanTwoTimes();
    }

    private void pressButton4() {

        GameArray gameArray = new GameArray();
        gameArray.deathDoor();
        gameArray.openDoor();
    }



    private void outputMenu() {
        System.out.println("\nAll array tasks");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select task which you wont to check");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
