package menko.controller;

import menko.controller.Task4.GameArray;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Task3 {
    private static Logger log = LogManager.getLogger(Task3.class);

    public static void deleteElementWhichMeetMoreThanTwoTimes() {
        int[] a = {1, 1, 2, 4, 5, 5, 6, 7};
        int[] result = new int[a.length];//optimize List
        int index = 0;

        System.out.println("arr" + Arrays.toString(a));

        for (int i = 0; i < a.length; i++) {
            if (i == 0) {
                result[index] = a[i];
                index++;
            }
            if (i == 1 && a[i] != a[i - 1] && a[i] != a[i + 1]) {
                result[index] = a[i];
                index++;
            }
            if (i > 1 && a[i] != a.length - 1 && a[i] != a[i + 1]) {
                result[index] = a[i];
                index++;
            }
        }

        System.out.println("arr" + Arrays.toString(result)); //List.asArray -> return as arr
        log.info("arr" + Arrays.toString(result));
    }



}
