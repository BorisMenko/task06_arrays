package menko.controller.Task4;

public class Artifact  extends WorldPower{


    public Artifact(int index, int powerLvl) {
        super("Artifact", index, powerLvl);
    }

    @Override
    public String toString() {
        return "Door Number " + getIndex() +" Artifact{" +
                "name='" + getName() + " power level= " + getPowerLvl() + '\'' +
                '}';
    }
}
