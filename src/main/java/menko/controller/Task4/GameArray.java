package menko.controller.Task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Random;

public class GameArray {
    private static Logger log = LogManager.getLogger(GameArray.class);
    Hero hero = new Hero();

    Random rand;

    public List<WorldPower> passDoors() {
        rand = new Random();

        List<WorldPower> doorList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            int generateObject = rand.nextInt((2 - 1) + 1) + 1;

            if (generateObject == 1) {
                int generateArtifactPower = rand.nextInt((80 - 10) + 1) + 10;
                doorList.add(new Artifact(i + 1, generateArtifactPower));
            } else {
                int generateMonsterPower = rand.nextInt((100 - 5) + 1) + 5;
                doorList.add(new Monster(i + 1, generateMonsterPower));
            }
        }

        return doorList;
    }

    public void deathDoor() {
        int countDeathDoor = 0;

        for (WorldPower s : passDoors()) {
            System.out.println(s);
        }

        for (WorldPower s : passDoors()) {
            if (s.getName() == "Monster") {
                if (s.getPowerLvl() > hero.getPower()) {
                    System.out.println(s.getIndex() + " This door danger");
                    countDeathDoor++;
                } else System.out.println(s.getIndex() + " This safe door");
            }
            if (s.getName() == "Artifact") {
                System.out.println(s.getIndex() + " This safe door");
            }

        }
        System.out.println("There is " + countDeathDoor + " death door");
    }



    public  void openDoor() {
        List<WorldPower> queDoors = new ArrayList<>();
        List<WorldPower> dangerDoor = new ArrayList<>();

        for (WorldPower s : passDoors()) {
            if (s.getName() == "Monster") {
                if (s.getPowerLvl() > hero.getPower()) {
                    dangerDoor.add(s);
                }
                if (s.getPowerLvl() <= hero.getPower()) {
                    queDoors.add(s);
                }

            }
            if (s.getName() == "Artifact") {
                hero.setPower(s.getPowerLvl());
                queDoors.add(s);
            }
        }

        try {
            for (WorldPower e : dangerDoor) {
                if (e.getPowerLvl() <= hero.getPower()) {
                    queDoors.add(e);
                    dangerDoor.remove(e);
                }
            }
        } catch (ConcurrentModificationException e) {
            e.printStackTrace();
        }


        System.out.println("This is safe door open procedure");
        log.info("This is safe door open procedure");
        for (WorldPower s : queDoors) {
            System.out.print(s.getIndex() + " ");
            log.info(s.getIndex() + " ");
        }


        if (dangerDoor.size() > 0) {
            System.out.println("\n" + "So strong monster for our hero in room number : ");
            for (WorldPower s : dangerDoor) {
                System.out.print(s.getIndex() + " ");
            }

        }

        System.out.println("\nAll numbers are random");

    }



}
