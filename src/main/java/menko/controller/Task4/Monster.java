package menko.controller.Task4;

public class Monster extends WorldPower {

    public Monster(int index, int powerLvl) {
        super("Monster", index, powerLvl);
    }

    @Override
    public String toString() {
        return "Door number "+ getIndex() + " Monster{" +
                "name='" + getName() + " power level= "+ getPowerLvl()  +'\'' +
                '}';
    }
}
