package menko.controller.Task4;

public class WorldPower {

    private String name;
    private int index;
    private int powerLvl;

    public WorldPower(String name, int index, int powerLvl) {
        this.name = name;
        this.index = index;
        this.powerLvl = powerLvl;
    }

    public int getPowerLvl() {
        return powerLvl;
    }

    public void setPowerLvl(int powerLvl) {
        this.powerLvl = powerLvl;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "WorldPower{" +
                "name='" + name + '\'' +
                ", index=" + index +
                ", powerLvl=" + powerLvl +
                '}';
    }
}
