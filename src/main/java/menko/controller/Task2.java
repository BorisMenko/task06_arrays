package menko.controller;

import com.sun.org.apache.regexp.internal.RE;
import menko.controller.Task4.GameArray;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Task2 {
    private static Logger log = LogManager.getLogger(Task2.class);
    Random rn;

    public  List<Integer> arrayIntegerNumbers(){
        rn = new Random();

        List someNumbers = new ArrayList();

        for (int i = 0; i < 30; i++){
            int randomNumber = rn.nextInt(10 - 1 + 1) + 1 ;
            someNumbers.add(randomNumber);
        }
        return someNumbers;
    }

    public List<Integer> numbersWithOutRapid(List<Integer> randomNumbers, int moreThan) {
        Map<Integer, Integer> numberOccurrenceMap = new HashMap();

        for (int i = 0; i < randomNumbers.size(); i++) {
            int number = randomNumbers.get(i);
            int count;

//            int count = numberOccurrenceMap.containsKey(number) ? (numberOccurrenceMap.get(number) + 1) : 1;

            if ( numberOccurrenceMap.containsKey(number)){
                count = (numberOccurrenceMap.get(number) +1 );
            } else {
                count = 1;
            }
            numberOccurrenceMap.put(number, count);
        }

        System.out.println(numberOccurrenceMap);

        for (Map.Entry<Integer, Integer> entry : numberOccurrenceMap.entrySet()) {
            int occurrence = entry.getValue();
            if(occurrence > moreThan) {
                randomNumbers.removeIf((item) -> (item == entry.getKey()));
            }
        }

        return randomNumbers;
    }

    public static void view(){
        int a = 2;
        Task2 task2 = new Task2();
        List<Integer> array = task2.arrayIntegerNumbers();

        System.out.println(array);
        log.info(array);

        System.out.println(task2.numbersWithOutRapid(array, a));
        log.info(task2.numbersWithOutRapid(array, a));
    }


}
