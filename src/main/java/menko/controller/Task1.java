package menko.controller;

import menko.controller.Task4.GameArray;
import menko.model.Candy;
import menko.model.Fruit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task1 {

    private static Logger log = LogManager.getLogger(Task1.class);
    Scanner sc;

    public static List<Candy> makeArraySweets(){
        List<Candy> candies = new ArrayList<Candy>();
        candies.add(new Candy("Sneakers","Chocolate bar"));
        candies.add(new Candy("Mars","Chocolate Bar"));
        candies.add(new Candy("Skittles","Caramel drops"));
        candies.add(new Candy("M&M's","Chocolate drops"));
        candies.add(new Candy("7days","Croissant"));

        return candies;
    }

    public static List<Fruit> makeArrayFruit(){
        List<Fruit> fruits = new ArrayList<Fruit>();
        fruits.add(new Fruit("Banana","Tropic fruit"));
        fruits.add(new Fruit("Apple","Most popular fruit"));
        fruits.add(new Fruit("Strawberry","Berry"));
        fruits.add(new Fruit("Mango","Sweet fruit"));
        fruits.add(new Fruit("Kiwi","Sour fruit"));

        return fruits;
    }

    public static List connectTwoArray(){
        List candiesAndFruits = new ArrayList();
        candiesAndFruits.add("Array with candies and fruits");

        for (int i = 0; i < makeArraySweets().size(); i++){
            candiesAndFruits.add(makeArraySweets().get(i));
        }

        for (int j = 0; j < makeArrayFruit().size(); j++){
            candiesAndFruits.add(makeArrayFruit().get(j));
        }

        return candiesAndFruits;

    }

    public static List selectWhichArrayToAdd(List fruitOrCandy){
        List fruitsOrCandies = new ArrayList();
        fruitOrCandy.add("");

        for (int i = 0; i < fruitOrCandy.size(); i ++){
            fruitsOrCandies.add(fruitOrCandy.get(i));
        }

        return fruitsOrCandies;
    }


    public void view(){
        sc = new Scanner(System.in);
        System.out.println("This is fruit array " + "\n" + makeArrayFruit() );
        System.out.println("This is candies array " + "\n" + makeArraySweets());

        System.out.println("");
        System.out.println(connectTwoArray());

        System.out.println("If you wont put candies enter number 1 or if fruits enter number 2");

        int a = sc.nextInt();
        if (a == 1){
            System.out.println(selectWhichArrayToAdd(makeArrayFruit()));
            log.info(selectWhichArrayToAdd(makeArrayFruit()));
        } else {
            System.out.println(selectWhichArrayToAdd(makeArraySweets()));
            log.info(selectWhichArrayToAdd(makeArraySweets()));
        }



    }


}
