package menko.model;

public class Candy {
    private String name;
    private String type;

    public Candy() {
    }

    public Candy(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Candy{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}' + "\n";
    }
}
